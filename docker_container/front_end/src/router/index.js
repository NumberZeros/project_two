import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/HomePage.vue'
const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: Home
  },
  {
    path: '/guest/login',
    name: 'GuestLogin',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/GuestLogin.vue')
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: () => import('../views/ForgotPassword.vue')
  },
  {
    path: '/set-new-password',
    name: 'SetNewPassword',
    component: () => import('../views/SetNewPassword.vue')
  },
  {
    path: '/user/setting',
    name: 'UserSetting',
    component: () => import('../views/UserSetting.vue')
  },
  {
    path: '/courses',
    name: 'Courses',
    component: () => import('../views/Courses.vue')
  },
  {
    path: '/courses/all',
    name: 'Catalog',
    component: () => import('../views/Catalog.vue')
  },
  {
    path: '/courses/id',
    name: 'Lessons',
    component: () => import('../views/Lessons.vue')
  },
  {
    path: '/courses/id/session',
    name: 'Session',
    component: () => import('../views/Sessions.vue'),
    props: true
  },
  {
    path: '/admin',
    name: 'AdminDashboard',
    component: () => import('../views/AdminDashboard.vue'),
  },
  {
    path: '/admin/manage-course',
    name: 'ManageCourse',
    component: () => import('../views/ManageCourse.vue')
  },
  {
    path: '/admin/manage-speaker',
    name: 'ManageSpeaker',
    component: () => import('../views/ManageSpeaker.vue')
  },
  {
    path: '/admin/manage-user',
    name: 'ManageUser',
    component: () => import('../views/ManageUser.vue')
  },
  {
    path: '/login',
    name: 'AdminSpeakerLogin',
    component: () => import('../views/AdminSpeakerLogin.vue')
  },
  {
    path: '/speaker',
    name: 'SpeakerDashboard',
    component: () => import('../views/SpeakerDashboard.vue')
  },
  {
    path: '/speaker/lesson/:idCourse',
    name: 'SpeakerManageLesson',
    component: () => import('../views/SpeakerManageLesson.vue')
  },
  {
    path: '/contact-us',
    name: 'ContactUs',
    component: () => import('../views/ContactUs.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

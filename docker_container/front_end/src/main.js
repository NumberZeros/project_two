import { createApp } from 'vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.bundle'

import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primevue/resources/themes/bootstrap4-dark-blue/theme.css';
import 'primeflex/primeflex.css';

import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';
import VueYouTubeIframe from '@techassi/vue-youtube-iframe';

let app = createApp(App).use(router)
app.use(PrimeVue)
app.use(VueYouTubeIframe)
app.config.globalProperties.$companyName = "Udacity"
app.config.globalProperties.$token = null
app.config.globalProperties.$role = null
app.config.globalProperties.$codeForResetPassword = null
app.mount('#app')
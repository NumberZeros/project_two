import axios from 'axios';
import { getCurrentInstance } from 'vue'

export default class UsersAdmin {
    constructor() {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        this.instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        })
    }

    getAll() {
        var result = []
        result = this.instance.get('admin/manage-user?limit=5&page=0')

        return result.then(function(value) {
            var data = []
            for(const i of value.data.data) {
                if(i.gender === 0) {
                    i.gender = 'Male'
                } else {
                    i.gender = 'Female'
                }
                if(i.isDelete === false) data.push(i)
            }

            return data
        })
    }

    ban(Name, Email, Phone, Gender, AccountType, id, _createAt ,Password) {
        let processedGender;
        if(Gender === 'Male') processedGender = 0
        else processedGender = 1

        this.instance.delete('admin/manage-user', {
            data: {
                gender: processedGender,
                isDelete: false,
                role: "user",
                typeAccount: AccountType,
                createAt: _createAt,
                _id: id,
                name: Name,
                email: Email,
                password: Password,
                phone: Phone,
                __v: 0
            }
        })
    }

    create(Name, Email, Password, Phone) {
        this.instance.post('admin/manage-user', {
            name: Name,
            email: Email,
            password: Password,
            phone: Phone
        })
    }

    getAllRequestSpeaker() {
        let result = []
        result = this.instance.get('admin/get-all-require-speaker?limit=5&page=0')
        return result.then(function(value) {
            return value.data.data.data
        })
    }

    validateRequestSpeaker(id) {
        this.instance.post('admin/validate-request-speaker', {
            _id: id
        })
    }
}

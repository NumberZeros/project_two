import axios from 'axios';
import { getCurrentInstance } from 'vue'

export default class LessonsSpeaker {
    constructor(idCourse) {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        this.instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        })
        this.idCourse = idCourse
    }

    getAll() {
        let result = []
        result = this.instance.get('speaker/lessons?idCourse='+ this.idCourse)
        return result.then(function(value) {
            console.log(value)
            return value.data.data
        })
    }

    create(Name, Description, Note, Link) {
        console.log(Link)
        this.instance.post('speaker/lessons', {
            name: Name,
            description: Description,
            note: Note,
            link: Link,
            idCourse: this.idCourse
        })
    }

    update(Id, Name, Description, Note, Link) {
        this.instance.put('speaker/lessons', {
            _id: Id,
            name: Name,
            description: Description,
            note: Note,
            link: Link,
            idCourse: this.idCourse
        })
    }

    delete(id) {
        this.instance.delete('speaker/lessons', {
            data: {
                _id : id
            }
        })
    }
}

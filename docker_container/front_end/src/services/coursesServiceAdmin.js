import axios from 'axios';
import { getCurrentInstance } from 'vue'
require("dotenv").config();
export default class CoursesAdmin {
    constructor() {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        console.log(this.token)
        this.instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        })
    }

    getAll() {
        let result = []
        result = this.instance.get('admin/courses?limit=10&page=0')
        return result.then(function(value) {
            return value.data.data.data
        })
    }

    validate(Id, Name, Description, Note) {
        this.instance.post('admin/courses', {
            _id: Id,
            name: Name,
            description: Description,
            note: Note,
            isValidate: false
        })
    }

    delete(id) {
        this.instance.delete('admin/courses', {
            data: {
                _id : id
            }
        })
    }
}

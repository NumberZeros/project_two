import axios from 'axios';
import { getCurrentInstance } from 'vue'
require("dotenv").config();
export default class LessonsAdmin {
    constructor() {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        this.instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        })
    }

    getAll() {
        let result = []
        result = this.instance.get('admin/lessons?limit=10&page=0')
        return result.then(function(value) {
            return value.data.data.data
        })
    }

    getLessonsOfCourse(id) {
        let result = []
        result = this.instance.get('admin/lessons?limit=10&page=0')
        return result.then(function(value) {
            let lessons = []
            for(const i of value.data.data) {
                if(i.idCourse._id === id) lessons.push(i)
            }
            return lessons
        })
    }

    delete(id) {
        this.instance.delete('speakers/lessons', {
            data: {
                _id : id
            }
        })
    }
}

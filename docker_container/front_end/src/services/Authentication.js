import axios from "axios";
import { getCurrentInstance } from "vue";
require("dotenv").config();
export default class AuthService {
    constructor() {
        this.instance = axios.create({
            baseURL:  "http://localhost:81"
        })
        this.internalInstance = getCurrentInstance()
    }

  login(Email, Password) {
    let result = [];
    result = this.instance.post("auth/login", {
      email: Email,
      password: Password,
    });
    return result
      .then((value) => {
        return value.data;
      })
      .catch((err) => {
        if (err.response) {
          return null;
        }
      });
  }

  updateInfo(Id, Name, Email, Phone, Gender) {
    this.instance.put("auth/update-info", {
      header: {
        Authencation: this.internalInstance.appContext.config.globalProperties
          .$token,
      },
      data: {
        _id: Id,
        name: Name,
        email: Email,
        phone: Phone,
        gender: Gender,
      },
    });
  }

  resetPassword(Email) {
    return this.instance
      .post("auth/reset-password", {
        header: {
          Authencation: this.internalInstance.appContext.config.globalProperties
            .$token,
        },
        data: {
          email: Email,
        },
      })
      .then((value) => value.code);
  }

  newPassword(Code, NewPassword) {
    this.instance.put("auth/new-password", {
      header: {
        Authencation: this.internalInstance.appContext.config.globalProperties
          .$token,
      },
      data: {
        code: Code,
        newPassword: NewPassword,
      },
    });
  }

  register(Name, Email, Password) {
    return this.instance
      .post("auth/register", {
        data: {
          name: Name,
          email: Email,
          password: Password,
        },
      })
      .then((value) => value.data);
  }
}

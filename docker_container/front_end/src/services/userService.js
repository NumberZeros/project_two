import axios from 'axios';
import { getCurrentInstance } from 'vue'

export default class User {
    constructor() {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        this.instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        })
    }

    requestSpeaker(Title, Description) {
        this.instance.post('user/request-speaker', {
            title: Title,
            description: Description
        })
    }

    joinCourse(IdCourse) {
        this.instance.post('user/join-courses', {
            idCourse: IdCourse
        })
    }

    getAllCourse(limit = 2, page = 0) {
        let result = this.instance.get('user/get-all-courses?limit='+limit+'&page='+page)
        return result.then(value => {
            return value.data.data.data
        })
    }

    getLesson() {
        let result = this.instance.get('user/get-my-courses')
        return result.then(value => {
            return value.data.data
        })
    }
}

import axios from 'axios';
import { getCurrentInstance } from 'vue'
require("dotenv").config();
export default class CoursesSpeaker {
    constructor() {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        this.instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        })
    }

    getAll() {
        let result = []
        result = this.instance.get('speaker/courses')
        return result.then(function(value) {
            return value.data.data
        })
    }

    create(Name, Description, Note) {
        this.instance.post('speaker/courses', {
            name: Name,
            description: Description,
            note: Note
        })
    }

    update(Id, Name, Description, Note) {
        this.instance.put('speaker/courses', {
            _id: Id,
            name: Name,
            description: Description,
            note: Note,
        })
    }

    delete(id) {
        this.instance.delete('speaker/courses', {
            data: {
                _id : id
            }
        })
    }
}

import axios from 'axios';
import { getCurrentInstance } from 'vue'
require("dotenv").config();
export default class Speakers {
    constructor() {
        const internalInstance = getCurrentInstance()
        this.token = internalInstance.appContext.config.globalProperties.$token
        
    }

    //Default
    getAll() {
        var result = []
        const instance = axios.create({
            baseURL: "http://localhost:81",
        });
        result = instance.get('http://localhost:80/admin/manage-speaker?limit=10&page=0', {
            headers: {
                Authorization: this.token
            }
        })
        return result.then(function(value) {
            var data = []
            for(const i of value.data.data.data) {
                if(i.gender === 0) {
                    i.gender = 'Male'
                } else {
                    i.gender = 'Female'
                }
                if(i.isDelete === false) data.push(i)
            }

            return data
        })
    }

    getAllWithPagination(_limit, _page) {
        var result = []
        const instance = axios.create({
            baseURL: "http://localhost:81",
        });
        result = instance.get('http://localhost:80/admin/manage-speaker', {
            headers: {
                Authorization: this.token
            },
            params: {
                limit: _limit,
                page: _page
            }
        })
        return result.then(function(value) {
            var data = []
            for(const i of value.data.data.data) {
                if(i.gender === 0) {
                    i.gender = 'Male'
                } else {
                    i.gender = 'Female'
                }
                if(i.isDelete === false) data.push(i)
            }

            return data
        })
    }

    create(Name, Email, Password, Phone) {
        const instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        });
        instance.post('/admin/manage-speaker', {
                name: Name,
                email: Email,
                password: Password,
                phone: Phone
        })
    }

    update(Name, Email, Phone, Gender, AccountType, id, _createAt ,Password) {
        let processedGender;
        if(Gender === 'Male') processedGender = 0
        else processedGender = 1

        const instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        });
        instance.put('/admin/manage-speaker', {
            gender: processedGender,
            isDelete: false,
            role: "speaker",
            typeAccount: AccountType,
            createAt: _createAt,
            _id: id,
            name: Name,
            email: Email,
            password: Password,
            phone: Phone,
            __v: 0
        })
    }

    delete(Name, Email, Phone, Gender, AccountType, id, _createAt ,Password) {
        let processedGender;
        if(Gender === 'Male') processedGender = 0
        else processedGender = 1

        const instance = axios.create({
            baseURL: "http://localhost:81",
            headers: {Authorization: this.token}
        });
        instance.delete('/admin/manage-speaker', {
            data: {
            gender: processedGender,
            isDelete: false,
            role: "speaker",
            typeAccount: AccountType,
            createAt: _createAt,
            _id: id,
            name: Name,
            email: Email,
            password: Password,
            phone: Phone,
            __v: 0
            }
        })
    }
}

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RequestStoreSchema = new Schema({
  idAccount: {
    type: Schema.Types.ObjectId,
    ref: "Accounts",
  },
  isValidate: {
    type: Boolean,
    default: false,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    default: false,
  },
  phone: {
    type: Number,
  },
  address: {
    type: String,
  },
  typeStore: {
    type: String,
  },
  isDelete: {
    type: Boolean,
    default: false,
  },
  createAt: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("RequestStores", RequestStoreSchema);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const StoreSchema = new Schema({
  idAccount: {
    type: Schema.Types.ObjectId,
    ref: "Accounts",
  },
  name: {
    type: String,
    default: false,
  },
  phone: {
    type: Number,
  },
  address: {
    type: String,
  },
  typeStore: {
    type: String,
  },
  isDelete: {
    type: Boolean,
    default: false,
  },
  createAt: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Stores", StoreSchema);

const service = require("./services");
const Token = require("../../../packages/Token");
const messages = require("../../../messages/courses.messages");

module.exports = {
  async registerStores(req, res) {
    try {
      const { headers, body } = req;
      const token = await Token.Authencatetion(headers);
      if (!token) throw { message: messages.NOT_LOGIN };
      const data = await service.registerStores({ _id: token._id, ...body });
      return res.send({
        success: true,
        data,
      });
    } catch (err) {
      return res.status(400).send({
        message: err,
      });
    }
  },
  async getAllRequestStore(req, res) {
    try {
      const { headers, query } = req;
      const token = await Token.Authencatetion(headers);
      if (!token) throw { message: messages.NOT_LOGIN };
      else if (token.role !== "admin")
        throw { message: messages.NOT_ALLOW_YOUR_ROLE };
      const { data, pagination } = await service.getAllRequestStore(query);
      return res.send({
        status: "success",
        data,
        pagination,
      });
    } catch (err) {
      return res.status(400).send({
        status: "failure",
        messages: err,
      });
    }
  },
  async getAllStore(req, res) {
    try {
      const { headers, query } = req;
      const token = await Token.Authencatetion(headers);
      if (!token) throw { message: messages.NOT_LOGIN };
      else if (token.role !== "admin")
        throw { message: messages.NOT_ALLOW_YOUR_ROLE };
      const { data, pagination } = await service.getAllStore(query);
      return res.send({
        status: "success",
        data,
        pagination,
      });
    } catch (err) {
      return res.status(400).send({
        status: "failure",
        messages: err,
      });
    }
  },
  async update(req, res) {
    try {
      const { body, headers } = req;
      const token = await Token.Authencatetion(headers);
      if (!token) throw { message: messages.NOT_LOGIN };
      else if (token.role !== "admin")
        throw { message: messages.NOT_ALLOW_YOUR_ROLE };
      const data = await service.update(body);
      res.send({
        success: true,
        ...data,
      });
    } catch (err) {
      if (err.name)
        return res.status(400).send({
          status: err.messages,
        });
      return res.status(400).send({
        status: err,
      });
    }
  },
  async delete(req, res) {
    try {
      const { body, headers } = req;
      const token = await Token.Authencatetion(headers);
      if (!token) throw { message: messages.NOT_LOGIN };
      else if (token.role !== "admin")
        throw { message: messages.NOT_ALLOW_YOUR_ROLE };
      const data = await service.delete(body);
      return res.send(data);
    } catch (err) {
      return res.status(400).send({
        status: err,
      });
    }
  },
};

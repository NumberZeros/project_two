const express = require("express");
const router = express.Router();
const controler = require("./controller");

const admin = "admin";

router.post(`/pick/registerStore`, controler.registerStores);

router.get(
  `/pick/${admin}/get-all-request-store`,
  controler.getAllRequestStore
);
router.put(`/pick/${admin}/validate-request-store`, controler.update);

router.get(`/pick/${admin}/manager-store`, controler.getAllStore);

router.delete(`/pick/${admin}/manage-store`, controler.delete);

module.exports = router;

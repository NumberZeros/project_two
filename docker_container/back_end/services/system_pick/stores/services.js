/* eslint-disable no-useless-catch */
require("../../../Schema/accounts");
require("../../../Schema/requestStore");
require("../../../Schema/stores");

const mongoose = require("mongoose");
const Accounts = mongoose.model("Accounts");
const RequestSpeaker = mongoose.model("RequestSpeaker");
const RequestStores = mongoose.model("RequestStores");
const Stores = mongoose.model("Stores");
const _ = require("lodash");

const messages = require("../../../messages/accounts.messages");
const packages = require("../../../packages/Common");
const { isEmpty } = require("lodash");

module.exports = {
  async registerStores({
    _id,
    title,
    description,
    name,
    phone,
    address,
    typeStore,
  }) {
    try {
      const data = await new RequestStores({
        idAccount: _id,
        title,
        description,
        name,
        phone,
        address,
        typeStore,
      }).save();
      console.log(data);
      return data;
    } catch (err) {
      throw err;
    }
  },
  async getAllRequestStore(query) {
    try {
      if (!_.isEmpty(query)) {
        const {
          limit,
          page,
          nextPage,
          search,
          skip,
        } = packages.DevicePagination(query);
        const data = await RequestSpeaker.find({
          isDelete: false,
        })
          .populate("idAccount")
          .limit(limit)
          .skip(skip)
          .exec();
        const alldocs = await RequestSpeaker.countDocuments({
          isDelete: false,
        }).exec();
        const totalPage = parseInt(alldocs / limit) + 1;
        return {
          data,
          pagination: {
            limit,
            page,
            nextPage: nextPage === totalPage ? null : nextPage,
            search,
            totalPage,
          },
        };
      } else {
        const data = await RequestSpeaker.find({
          isDelete: false,
        })
          .populate("idAccount")
          .exec();
        return {
          data,
          pagination: null,
        };
      }
    } catch (err) {
      throw err;
    }
  },
  async getAllStore(query) {
    try {
      if (!_.isEmpty(query)) {
        const {
          limit,
          page,
          nextPage,
          search,
          skip,
        } = packages.DevicePagination(query);
        const data = await Stores.find({
          isDelete: false,
        })
          .populate("idAccount")
          .limit(limit)
          .skip(skip)
          .exec();
        const alldocs = await Stores.countDocuments({
          isDelete: false,
        }).exec();
        const totalPage = parseInt(alldocs / limit) + 1;
        return {
          data,
          pagination: {
            limit,
            page,
            nextPage: nextPage === totalPage ? null : nextPage,
            search,
            totalPage,
          },
        };
      } else {
        const data = await RequestSpeaker.find({
          isDelete: false,
        })
          .populate("idAccount")
          .exec();
        return {
          data,
          pagination: null,
        };
      }
    } catch (err) {
      throw err;
    }
  },
  async save({ name, email, password, phone, address, birthdate, gender }) {
    try {
      const data = await new Accounts({
        name,
        email,
        password,
        phone,
        address,
        birthdate,
        gender,
        role: "speaker",
        typeAccount: "free",
      }).save();
      return {
        data: data,
      };
    } catch (err) {
      const { keyValue } = err;
      if (keyValue.email) throw messages.EMAIL_EXIST;
      throw err;
    }
  },
  async update({ isValidate, idForm }) {
    try {
      const dataRequest = await RequestStores.findOneAndUpdate(
        {
          _id: idForm,
          isValidate: false,
          isDelete: false,
        },
        {
          isValidate,
          isDelete: true,
        },
        {
          new: true,
        }
      )
        .populate("idAccount")
        .exec();
      if (isEmpty(dataRequest))
        throw {
          name: "not valid request",
          messages:
            "This request form not valid or form is validate after remove",
        };
      const data = await new Stores({
        name: dataRequest.name,
        phone: dataRequest.phone,
        address: dataRequest.address,
        typeStore: dataRequest.typeStore,
      }).save();

      return {
        data,
        dataRequest,
      };
    } catch (err) {
      throw err;
    }
  },
  async delete({ _id }) {
    try {
      const data = await Stores.findOneAndUpdate(
        { _id: _id },
        { isDelete: true },
        {
          new: true,
          upsert: true,
          rawResult: true,
        }
      );
      if (!data)
        throw { name: "not found store", messages: "This store not found" };
      return {
        data: data.value,
      };
    } catch (err) {
      throw err;
    }
  },
};

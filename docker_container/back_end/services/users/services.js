/* eslint-disable no-useless-catch */
require("../../Schema/requestSpeaker");
require("../../Schema/courses");
require("../../Schema/lessons");
require("../../Schema/accounts");

const mongoose = require("mongoose");
const RequestSpeaker = mongoose.model("RequestSpeaker");
const Courses = mongoose.model("Courses");
const Lessons = mongoose.model("Lessons");
const Accounts = mongoose.model("Accounts");

const _ = require("lodash");

const packages = require("../../packages/Common");
const messages = require("../../messages/accounts.messages");
const courses = require("../../Schema/courses");
module.exports = {
  async requestSpeaker({ idAccount, title, description }) {
    try {
      const data = await RequestSpeaker({
        idAccount,
        title,
        description,
      }).save();
      return data;
    } catch (err) {
      throw err;
    }
  },
  async findAllMyCourses({ idAccount, query }) {
    try {
      if (!_.isEmpty(query)) {
        const {
          limit,
          page,
          nextPage,
          // search,
          skip,
        } = packages.DevicePagination(query);
        const filter = query.name
          ? {
              isDelete: false,
              $or: [
                {
                  name: query.name
                    ? { $regex: `^${query.name}`, $options: "i" }
                    : null,
                },
              ],
            }
          : { isDelete: false };

        const data = await Lessons.find(filter)
          .limit(limit)
          .skip(skip)
          .populate({
            path: "idCourse",
            match: { isDelete: false },
          })
          .exec();
        const alldocs = await Lessons.countDocuments(filter)
          .populate({
            path: "idCourse",
            match: { isDelete: false },
          })
          .exec();
        const totalPage = parseInt(alldocs / limit) + 1;
        return {
          data,
          pagination: {
            limit,
            page,
            nextPage: nextPage === totalPage ? null : nextPage,
            totalPage,
          },
        };
      } else {
        const data = await Lessons.find({ isDelete: false })
          .populate({
            path: "idCourse",
            match: { isDelete: false },
          })
          .exec();
        return data;
      }
    } catch (err) {
      throw err;
    }
  },
  async findAllCourses(query) {
    try {
      if (!_.isEmpty(query)) {
        const {
          limit,
          page,
          nextPage,
          search,
          skip,
        } = packages.DevicePagination(query);

        const filter = query.name
          ? {
              isDelete: false,
              $or: [
                {
                  name: query.name
                    ? { $regex: `^${query.name}`, $options: "i" }
                    : null,
                },
              ],
            }
          : { isDelete: false };

        const data = await Courses.find(filter)
          .populate("idSpeaker")
          .limit(limit)
          .skip(skip)
          .exec();
        const alldocs = await Courses.countDocuments(filter).exec();
        const totalPage = parseInt(alldocs / limit) + 1;
        return {
          data,
          pagination: {
            limit,
            page,
            nextPage: nextPage === totalPage ? null : nextPage,
            search,
            totalPage,
          },
        };
      } else return await Courses.find({ isDelete: false });
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
  async joinCourse({ idAccount, idCourse }) {
    try {
      const isHaveAccount = await Accounts.findOne({ _id: idAccount }).exec();
      const isHaveCourses = await Courses.findOne({ _id: idCourse }).exec();
      if (_.isEmpty(isHaveAccount)) throw messages.NOT_FOUND_ACCOUNT;
      if (_.isEmpty(isHaveCourses)) throw messages.NOT_FOUND_COURSES;
      const data = await Courses.findOneAndUpdate(
        { _id: idCourse },
        {
          $push: { listStudent: idAccount },
        },
        { new: true }
      );
      return data;
    } catch (err) {
      throw err;
    }
  },
};
